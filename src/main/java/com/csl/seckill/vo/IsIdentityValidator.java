package com.csl.seckill.vo;

import com.csl.seckill.utils.IdentityUtil;
import com.csl.seckill.validator.IsIdentity;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsIdentityValidator implements ConstraintValidator<IsIdentity,String> {

    //获取身份证号是否为必填项
    private boolean required=false;

    @Override
    public void initialize(IsIdentity constraintAnnotation) {
        required=constraintAnnotation.required();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        //身份证号是否是必填项
        if (required) {
            if (StringUtils.isEmpty(value)) {
                return false;//空值无效
            } else
            return IdentityUtil.isLegalPattern(value);//判断身份证号是否是否正确
        }
        //手机号不是必填项则直接返回true，表示输入的数据有效
        else {
            return true;
        }
    }
}
