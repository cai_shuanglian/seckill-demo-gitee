package com.csl.seckill.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @Author:CaiShuangLian
 * @FileName:
 * @Date:Created in  2021/9/9 17:47
 * @Version:
 * @Description:公共返回对象枚举
 */
@Getter
@ToString
@AllArgsConstructor
public enum  RespBeanEnum {
//    通用
    SUCCESS(200,"SUCCESS"),
    ERROR(500,"服务端异常"),
//    登录
    LOGIN_ERROR(500210,"用户名或密码错误！"),
    MOBILE_ERROR(500211,"手机号码格式不正确"),
//    异常
    BIN_ERROR(500212,"参数校验异常"),

    //注册
    REGIST_ERROR(5003110,"注册失败"),
    DUPLICATEKEY_ERROR(5003111,"该手机号已被注册"),
    IDENTIFYNUMBER_ERROR(5003112,"身份证号格式错误"),

//    秒杀模块5005XX
    EMPTY_STOCK(500500,"库存不足"),
    REPEATE_ERROR(500501,"每人限购一件"),

    //不符合秒杀条件
    UNQUALIFIED(20001,"不符合参与活动条件"),
    OVERDUE_UNQUALIFIED(200002,"您有逾期未归还，请及时处理"),
    AGE_UNQUALIFIED(200003,"不符合参与该活动年龄")
    ;


    private final Integer code;
    private final String message;
}
