package com.csl.seckill.vo;

import com.csl.seckill.validator.IsIdentity;
import com.csl.seckill.validator.IsMobile;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class RegistVo {

    @NotNull
    @IsMobile(required = true,message = "手机号码格式错误")
    private String mobile;

    @NotNull
    @Length(min = 32)
    private String password;

    @NotNull
    private String nickname;

    /**
     * 身份证号
     */
    @NotNull
//    @IsIdentity(required = true,message = "身份证号出错")
    private String identifyNumber;

    /**
     * 用户真实姓名
     */
    @NotNull
    private String realName;

    /**
     * 在业/待业
     */
    @NotNull
    private Integer workStatus;

}
