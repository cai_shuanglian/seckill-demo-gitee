package com.csl.seckill.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * user实体类
 * </p>
 *
 * @author CaiShuangLian
 * @since 2021-09-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID，手机号码
     */
    @TableId("id")
    @NotNull
    private Long id;

    private String nickname;

    /**
     * MD5（MD5（pass明文+固定salt）+salt）
     */
    private String pwd;

    private String salt;

    /**
     * 头像
     */
    private String head;

    /**
     * 注册时间
     */
    private Date registerDate;

    /**
     * 最后一次登录时间
     */
    private Date lastLoginDate;

    /**
     * 登录次数
     */
    private Integer loginCount;

    /**
     * 身份证号
     */
    @NotNull
    private String identifyNumber;

    /**
     * 用户真实姓名
     */

    private String realName;

    /**
     * 在业/待业
     */
    @NotNull
    private Boolean workStatus;

    /**
     * 是否失信
     */
    @NotNull
    private Boolean creditStatus;


}
