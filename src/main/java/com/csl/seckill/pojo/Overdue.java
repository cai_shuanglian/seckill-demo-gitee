package com.csl.seckill.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author:CaiShuangLian
 * @FileName:
 * @Date:Created in  2022/3/17 14:42
 * @Version:
 * @Description:TODO
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_overdue")
public class Overdue implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 逾期开始时间（也就是最晚还款时间）
     */
    private Date startDate;

    /**
     * 预期截至时间（归还时间）
     */
    private Date endDate;

    /**
     * 逾期金额
     */
    private Double money;

    /**
     * 逾期状态（是否归还）
     */
    private Boolean status;

}
