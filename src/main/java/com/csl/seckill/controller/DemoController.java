package com.csl.seckill.controller;

import com.csl.seckill.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author:CaiShuangLian ChenTing
 * @FileName:
 * @Date:Created in  2021/9/9 9:47
 * @Version:
 * @Description:TODO
 */

@Controller
@RequestMapping("/demo")
public class DemoController {
@Autowired
private UserMapper userMapper;
    /**
     * 测试页面跳转
     * @param model
     * @return
     */
    @RequestMapping("/hello")
    public String hello(Model model){
        System.out.println(userMapper.selectById("13500000000"));
        model.addAttribute("name","csl");
        return "hello";
    }
}
