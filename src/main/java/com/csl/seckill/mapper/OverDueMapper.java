package com.csl.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.csl.seckill.pojo.Overdue;

import java.util.List;

public interface OverDueMapper extends BaseMapper<Overdue> {

    /**
     * 获取逾期状态（排除三天内还完1000以内）
     * @param userId
     * @return
     */
    List<Overdue> getOverdueStatus(Long userId);
}
