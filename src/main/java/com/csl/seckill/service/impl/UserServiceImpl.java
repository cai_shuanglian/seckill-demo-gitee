package com.csl.seckill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.csl.seckill.exception.GlobalException;
import com.csl.seckill.mapper.OverDueMapper;
import com.csl.seckill.mapper.UserMapper;
import com.csl.seckill.pojo.User;
import com.csl.seckill.service.IUserService;
import com.csl.seckill.utils.CookieUtil;
import com.csl.seckill.utils.IdentityUtil;
import com.csl.seckill.utils.MD5Util;
import com.csl.seckill.utils.UUIDUtil;
import com.csl.seckill.vo.LoginVo;
import com.csl.seckill.vo.RegistVo;
import com.csl.seckill.vo.RespBean;
import com.csl.seckill.vo.RespBeanEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author CaiShuangLian
 * @since 2021-09-09
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired(required = false)
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private OverDueMapper overDueMapper;

    /**
     * 登录功能
     * @param loginVo
     * @param request
     * @param response
     * @return
     */
    @Override
    public RespBean doLogin(LoginVo loginVo, HttpServletRequest request, HttpServletResponse response) {
        String mobile = loginVo.getMobile();
        String password = loginVo.getPassword();
        //根据手机号获取用户
        User user = userMapper.selectById(mobile);

        if(user==null){
            throw new GlobalException(RespBeanEnum.LOGIN_ERROR);
        }
        //判断密码是否正确
        if(!MD5Util.formPassToDBPass(password,user.getSalt()).equals(user.getPwd())){
            throw new GlobalException(RespBeanEnum.LOGIN_ERROR);
        }
        //判断是否是失信人或者失业
        if(Boolean.FALSE.equals(user.getWorkStatus() && user.getCreditStatus())){
            throw new GlobalException(RespBeanEnum.UNQUALIFIED);
        }
        //判断是否逾期
        if(overDueMapper.getOverdueStatus(user.getId()).size()>=2){
            throw new GlobalException(RespBeanEnum.OVERDUE_UNQUALIFIED);
        }

        //判断是否是小于十八岁,小于十八岁不能参与
        if(getAgeByIDNumber(user.getIdentifyNumber())<18){
            throw new GlobalException(RespBeanEnum.AGE_UNQUALIFIED);
        }

        //生成cookie
        String ticket= UUIDUtil.uuid();
        //将用户信息存入redis中
        redisTemplate.opsForValue().set("user:"+ticket,user);
        //将session存储在cookie中
        CookieUtil.setCookie(request,response,"userTicket",ticket);
        return RespBean.success(ticket);
    }

    @Override
    public RespBean doRegist(RegistVo registVo) {

        User user = new User();
        user.setId( Long.parseLong(registVo.getMobile()));
        user.setNickname(registVo.getNickname());
        user.setPwd(MD5Util.inputPassToFormPass(registVo.getPassword()));
        user.setSalt("1a2b3c4d");
        user.setIdentifyNumber(registVo.getIdentifyNumber());
        user.setRealName(registVo.getRealName());
        user.setWorkStatus(registVo.getWorkStatus()==1?true:false);
        user.setRegisterDate(new Date());
        user.setCreditStatus(true);
        System.out.println(user);
        try{
            userMapper.insert(user);
        }catch (DuplicateKeyException e){
            throw new GlobalException(RespBeanEnum.DUPLICATEKEY_ERROR);
        }catch (Exception e){
            throw new GlobalException(RespBeanEnum.REGIST_ERROR);
        }
        return RespBean.success();
    }

    /**
     * 根据cookie获取用户
     * @param userTicket
     * @param httpServletRequest
     * @param httpServletResponse
     * @return
     */
    @Override
    public User getUserByCookie(String userTicket, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        if(StringUtils.isEmpty(userTicket))
            return null;
        User user = (User) redisTemplate.opsForValue().get("user:" + userTicket);
        if(user!=null){
            //重新设置cookie，处于安全考量
            CookieUtil.setCookie(httpServletRequest,httpServletResponse,"userTicket",userTicket);
        }
        return user;
    }


    private static final int invalidAge = -1;//非法的年龄，用于处理异常。
    /**
     *根据身份证号码计算年龄
     * @param idNumber 考虑到了15位身份证，但不一定存在
     */
    public static int getAgeByIDNumber(String idNumber) {
        String dateStr;
        if (idNumber.length() == 15) {
            dateStr = "19" + idNumber.substring(6, 12);
        } else if (idNumber.length() == 18) {
            dateStr = idNumber.substring(6, 14);
        } else {//默认是合法身份证号，但不排除有意外发生
            return invalidAge;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        try {
            Date birthday = simpleDateFormat.parse(dateStr);
            return getAgeByDate(birthday);
        } catch (ParseException e) {
            return invalidAge;
        }
    }

    public static int getAgeByDate(Date birthday) {
        Calendar calendar = Calendar.getInstance();

        //calendar.before()有的点bug
        if (calendar.getTimeInMillis() - birthday.getTime() < 0L) {
            return invalidAge;
        }


        int yearNow = calendar.get(Calendar.YEAR);
        int monthNow = calendar.get(Calendar.MONTH);
        int dayOfMonthNow = calendar.get(Calendar.DAY_OF_MONTH);

        calendar.setTime(birthday);


        int yearBirthday = calendar.get(Calendar.YEAR);
        int monthBirthday = calendar.get(Calendar.MONTH);
        int dayOfMonthBirthday = calendar.get(Calendar.DAY_OF_MONTH);

        int age = yearNow - yearBirthday;
        if (monthNow <= monthBirthday && monthNow == monthBirthday && dayOfMonthNow < dayOfMonthBirthday || monthNow < monthBirthday) {
            age--;
        }
        return age;
    }



}
